﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cnsProjectOne
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.CursorVisible = false; // cursore off
            ConsoleKeyInfo k;

            // create random position for flag
            var rand = new Random();

            // coordinate of player
            int playerX = 15;
            int playerY = 15;
            char playerChar = 'X';

            // ccoordinate of flag
            int flagX = 25;
            int flagY = 15;

            // create score position
            int score = 0;
            int scoreX = 0;
            int scoreY = 0;
            //string scoreInfo = $"score: {score}";


            do
            {
                Console.Clear();
                Console.SetCursorPosition(scoreX, scoreY);
                string scoreInfo = $"score: {score}";
                Console.WriteLine(scoreInfo);

                if (score < 5)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.SetCursorPosition(flagX, flagY);
                    Console.Write("F"); // flag
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.SetCursorPosition(flagX, flagY);
                    Console.Write("@"); // flag
                }
                

                Console.SetCursorPosition(playerX, playerY);
                Console.Write(playerChar);


                k = Console.ReadKey(true);

                if (k.Key == ConsoleKey.UpArrow)
                    playerY--;
                else if (k.Key == ConsoleKey.DownArrow)
                    playerY++;
                else if (k.Key == ConsoleKey.LeftArrow)
                    playerX--;
                else if (k.Key == ConsoleKey.RightArrow)
                    playerX++;


                if (playerX == flagX && playerY == flagY)
                {

                    flagX = rand.Next(0, 20);
                    flagY = rand.Next(1, 15);

                    if (score < 5)
                    {
                        score += 1;
                    }
                    else
                    {
                        score += 3;
                    }
                   
                    Console.Clear();
                    // Console.Write("F"); // symbol
                }

                // Error catcher
                if (playerX < 1)
                {
                    playerX += 0;
                }
                else if (playerY < 1)
                {
                    playerY += 1;
                }

            } while (k.Key != ConsoleKey.Escape); // выходим из цикла по нажатию Esc

            Console.CursorVisible = true;
        }
    }
}
